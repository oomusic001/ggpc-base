package com.ggpc.base.dao;

import com.ggpc.base.entity.LogEntity;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 *  Mapper 接口
 * @author gey8866@126.com
 * @since 2021-06-08
 */
public interface LogDao extends BaseMapper<LogEntity> {

}
