package com.ggpc.base.entity;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ggpc.base.BaseEntity;

/**
 *  实体类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@TableName("permission")
public class PermissionEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Excel(name = "",width = 10)
    private Integer permissionId;

    @Excel(name = "",width = 10)
    private String permissionName;

    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }
    public String getPermissionName() {
        return permissionName;
    }

    public void setPermissionName(String permissionName) {
        this.permissionName = permissionName;
    }

    @Override
    public String toString() {
        return "PermissionEntity{" +
            "permissionId=" + permissionId +
            ", permissionName=" + permissionName +
        "}";
    }
}
