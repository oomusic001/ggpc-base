package com.ggpc.base.entity;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ggpc.base.BaseEntity;

/**
 *  实体类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@TableName("role_permission")
public class RolePermissionEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Excel(name = "",width = 10)
    private Integer roleId;

    @Excel(name = "",width = 10)
    private Integer permissionId;

    public Integer getRoleId() {
        return roleId;
    }

    public void setRoleId(Integer roleId) {
        this.roleId = roleId;
    }
    public Integer getPermissionId() {
        return permissionId;
    }

    public void setPermissionId(Integer permissionId) {
        this.permissionId = permissionId;
    }

    @Override
    public String toString() {
        return "RolePermissionEntity{" +
            "roleId=" + roleId +
            ", permissionId=" + permissionId +
        "}";
    }
}
