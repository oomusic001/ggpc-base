package com.ggpc.base.entity;
import cn.afterturn.easypoi.excel.annotation.Excel;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ggpc.base.BaseEntity;

/**
 *  实体类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@TableName("user_role")
public class UserRoleEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Excel(name = "",width = 10)
    private BigDecimal userId;

    @Excel(name = "",width = 10)
    private String roleId;

    public BigDecimal getUserId() {
        return userId;
    }

    public void setUserId(BigDecimal userId) {
        this.userId = userId;
    }
    public String getRoleId() {
        return roleId;
    }

    public void setRoleId(String roleId) {
        this.roleId = roleId;
    }

    @Override
    public String toString() {
        return "UserRoleEntity{" +
            "userId=" + userId +
            ", roleId=" + roleId +
        "}";
    }
}
