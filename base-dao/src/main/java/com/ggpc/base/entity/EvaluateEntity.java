package com.ggpc.base.entity;
import cn.afterturn.easypoi.excel.annotation.Excel;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ggpc.base.BaseEntity;

/**
 *  实体类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@TableName("evaluate")
public class EvaluateEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Excel(name = "",width = 10)
    private BigDecimal orderId;

    @Excel(name = "",width = 10)
    private BigDecimal userId;

    @Excel(name = "",width = 10)
    private String content;

    @Excel(name = "",width = 10)
    private Integer level;

    public BigDecimal getOrderId() {
        return orderId;
    }

    public void setOrderId(BigDecimal orderId) {
        this.orderId = orderId;
    }
    public BigDecimal getUserId() {
        return userId;
    }

    public void setUserId(BigDecimal userId) {
        this.userId = userId;
    }
    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }

    @Override
    public String toString() {
        return "EvaluateEntity{" +
            "orderId=" + orderId +
            ", userId=" + userId +
            ", content=" + content +
            ", level=" + level +
        "}";
    }
}
