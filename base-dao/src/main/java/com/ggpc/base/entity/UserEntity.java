package com.ggpc.base.entity;
import cn.afterturn.easypoi.excel.annotation.Excel;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import com.ggpc.base.BaseEntity;

/**
 *  实体类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@TableName("t_user")
public class UserEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

        /**
         * 手机号
         */
    @Excel(name = "手机号",width = 10)
    private BigDecimal mobile;

        /**
         * 身份证号
         */
    @Excel(name = "身份证号",width = 10)
    private String idCard;

        /**
         * 用户名
         */
    @Excel(name = "用户名",width = 10)
    private String userName;

        /**
         * 用户类型
         */
    @Excel(name = "用户类型",width = 10)
    private String userType;

        /**
         * 密码
         */
    @Excel(name = "密码",width = 10)
    private String password;





        /**
         * 头像
         */
    @Excel(name = "头像",width = 10)
    private String titleImg;

        /**
         * 用户状态
         */
    @Excel(name = "用户状态",width = 10)
    private Integer status;

        /**
         * 微信
         */
    @Excel(name = "微信",width = 10)
    private String vchat;

        /**
         * 盐
         */
    @Excel(name = "盐",width = 10)
    private String salt;

        /**
         * 邮箱
         */
    @Excel(name = "邮箱",width = 10)
    private String email;

        /**
         * 最后登录ip
         */
    @Excel(name = "最后登录ip",width = 10)
    private String lastLoginIp;

        /**
         * 最后登录时间
         */
    @Excel(name = "最后登录时间",width = 10)
    private Date lastLoginTime;

        /**
         * 生日
         */
    @Excel(name = "生日",width = 10)
    private Date birthday;

        /**
         * 车牌号
         */
    @Excel(name = "车牌号",width = 10)
    private Integer carNo;

        /**
         * 车品牌
         */
    @Excel(name = "车品牌",width = 10)
    private Integer carBrand;

    public BigDecimal getMobile() {
        return mobile;
    }

    public void setMobile(BigDecimal mobile) {
        this.mobile = mobile;
    }
    public String getIdCard() {
        return idCard;
    }

    public void setIdCard(String idCard) {
        this.idCard = idCard;
    }
    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }
    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }
    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }
    public String getTitleImg() {
        return titleImg;
    }

    public void setTitleImg(String titleImg) {
        this.titleImg = titleImg;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getVchat() {
        return vchat;
    }

    public void setVchat(String vchat) {
        this.vchat = vchat;
    }
    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }
    public String getLastLoginIp() {
        return lastLoginIp;
    }

    public void setLastLoginIp(String lastLoginIp) {
        this.lastLoginIp = lastLoginIp;
    }
    public Date getLastLoginTime() {
        return lastLoginTime;
    }

    public void setLastLoginTime(Date lastLoginTime) {
        this.lastLoginTime = lastLoginTime;
    }
    public Date getBirthday() {
        return birthday;
    }

    public void setBirthday(Date birthday) {
        this.birthday = birthday;
    }
    public Integer getCarNo() {
        return carNo;
    }

    public void setCarNo(Integer carNo) {
        this.carNo = carNo;
    }
    public Integer getCarBrand() {
        return carBrand;
    }

    public void setCarBrand(Integer carBrand) {
        this.carBrand = carBrand;
    }

    @Override
    public String toString() {
        return "UserEntity{" +
            "mobile=" + mobile +
            ", idCard=" + idCard +
            ", userName=" + userName +
            ", userType=" + userType +
            ", password=" + password +
            ", createTime=" + createTime +
            ", createBy=" + createBy +
            ", updateTime=" + updateTime +
            ", updateBy=" + updateBy +
            ", titleImg=" + titleImg +
            ", status=" + status +
            ", vchat=" + vchat +
            ", salt=" + salt +
            ", email=" + email +
            ", lastLoginIp=" + lastLoginIp +
            ", lastLoginTime=" + lastLoginTime +
            ", birthday=" + birthday +
            ", carNo=" + carNo +
            ", carBrand=" + carBrand +
        "}";
    }
}
