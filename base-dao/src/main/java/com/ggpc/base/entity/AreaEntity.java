package com.ggpc.base.entity;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ggpc.base.BaseEntity;

/**
 *  实体类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@TableName("area")
public class AreaEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

        /**
         * 名称
         */
    @Excel(name = "名称",width = 10)
    private String name;

        /**
         * 父级ID,0表示没有父级
         */
    @Excel(name = "父级ID,0表示没有父级",width = 10)
    private Integer pid;

        /**
         * 1表示省，2表示市，3表示县。
         */
    @Excel(name = "1表示省，2表示市，3表示县。",width = 10)
    private Integer level;

        /**
         * 编码
         */
    @Excel(name = "编码",width = 10)
    private String code;

        /**
         * 状态，默认为1
         */
    @Excel(name = "状态，默认为1",width = 10)
    private Integer status;

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public Integer getPid() {
        return pid;
    }

    public void setPid(Integer pid) {
        this.pid = pid;
    }
    public Integer getLevel() {
        return level;
    }

    public void setLevel(Integer level) {
        this.level = level;
    }
    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "AreaEntity{" +
            "name=" + name +
            ", pid=" + pid +
            ", level=" + level +
            ", code=" + code +
            ", status=" + status +
        "}";
    }
}
