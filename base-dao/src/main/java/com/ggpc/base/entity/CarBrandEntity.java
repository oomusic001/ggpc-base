package com.ggpc.base.entity;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ggpc.base.BaseEntity;

/**
 *  实体类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@TableName("car_brand")
public class CarBrandEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Excel(name = "",width = 10)
    private Integer brandParentId;

    @Excel(name = "",width = 10)
    private String brandName;

    public Integer getBrandParentId() {
        return brandParentId;
    }

    public void setBrandParentId(Integer brandParentId) {
        this.brandParentId = brandParentId;
    }
    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    @Override
    public String toString() {
        return "CarBrandEntity{" +
            "brandParentId=" + brandParentId +
            ", brandName=" + brandName +
        "}";
    }
}
