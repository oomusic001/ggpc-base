package com.ggpc.base.entity;
import cn.afterturn.easypoi.excel.annotation.Excel;
import com.baomidou.mybatisplus.annotation.TableName;
import com.ggpc.base.BaseEntity;

/**
 *  实体类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@TableName("dict")
public class DictEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

    @Excel(name = "",width = 10)
    private Integer type;

    @Excel(name = "",width = 10)
    private String name;

    @Excel(name = "",width = 10)
    private String description;

    @Excel(name = "",width = 10)
    private String status;

    public Integer getType() {
        return type;
    }

    public void setType(Integer type) {
        this.type = type;
    }
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    @Override
    public String toString() {
        return "DictEntity{" +
            "type=" + type +
            ", name=" + name +
            ", description=" + description +
            ", status=" + status +
        "}";
    }
}
