package com.ggpc.base.entity;
import cn.afterturn.easypoi.excel.annotation.Excel;
import java.math.BigDecimal;
import com.baomidou.mybatisplus.annotation.TableName;
import java.util.Date;
import com.ggpc.base.BaseEntity;

/**
 *  实体类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@TableName("orders")
public class OrdersEntity extends BaseEntity {

    private static final long serialVersionUID = 1L;

        /**
         * 始发地
         */
    @Excel(name = "始发地",width = 10)
    private BigDecimal fromId;

        /**
         * 终点
         */
    @Excel(name = "终点",width = 10)
    private BigDecimal toId;

        /**
         * 用户id
         */
    @Excel(name = "用户id",width = 10)
    private BigDecimal userId;

        /**
         * 始发时间
         */
    @Excel(name = "始发时间",width = 10)
    private Date fromTime;





        /**
         * 订单状态
         */
    @Excel(name = "订单状态",width = 10)
    private Integer status;

        /**
         * 经停地
         */
    @Excel(name = "经停地",width = 10)
    private String stayArea;

    public BigDecimal getFromId() {
        return fromId;
    }

    public void setFromId(BigDecimal fromId) {
        this.fromId = fromId;
    }
    public BigDecimal getToId() {
        return toId;
    }

    public void setToId(BigDecimal toId) {
        this.toId = toId;
    }
    public BigDecimal getUserId() {
        return userId;
    }

    public void setUserId(BigDecimal userId) {
        this.userId = userId;
    }
    public Date getFromTime() {
        return fromTime;
    }

    public void setFromTime(Date fromTime) {
        this.fromTime = fromTime;
    }
    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer status) {
        this.status = status;
    }
    public String getStayArea() {
        return stayArea;
    }

    public void setStayArea(String stayArea) {
        this.stayArea = stayArea;
    }

    @Override
    public String toString() {
        return "OrdersEntity{" +
            "fromId=" + fromId +
            ", toId=" + toId +
            ", userId=" + userId +
            ", fromTime=" + fromTime +
            ", createTime=" + createTime +
            ", createBy=" + createBy +
            ", updateTime=" + updateTime +
            ", updateBy=" + updateBy +
            ", status=" + status +
            ", stayArea=" + stayArea +
        "}";
    }
}
