package com.ggpc.base.controller;

import com.ggpc.base.service.EvaluateService;
import com.ggpc.base.entity.EvaluateEntity;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import java.io.IOException;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.GetMapping;
import org.apache.poi.ss.usermodel.Workbook;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import com.ggpc.base.web.advice.RespMessage;
import com.ggpc.base.web.advice.ResponseMessage;
import com.ggpc.base.web.util.FileUtil;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.Arrays;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 *  控制层
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@RestController
@RequestMapping("/evaluate")
public class EvaluateController {

    @Autowired
    private EvaluateService   evaluateService;
    /**
     * 新增
     */
     @PostMapping("/save")
     @RespMessage
     public ResponseMessage save(@RequestBody EvaluateEntity evaluateEntity){
            boolean success = evaluateService.save(evaluateEntity);
            if(success){
                return ResponseMessage.ok();
            }
            return ResponseMessage.error();
     }
    /**
     * 修改
     */
    @PostMapping("/update")
    @RespMessage
    public ResponseMessage update(@RequestBody EvaluateEntity evaluateEntity){
            boolean success = evaluateService.updateByPK(evaluateEntity);
            if(success){
                return ResponseMessage.ok();
            }
            return ResponseMessage.error().message("数据未更新或数据不存在");
     }
    /**
     * 删除
     */
    @PostMapping("/delete")
    @RespMessage
    public ResponseMessage delete(Long id){
            boolean success = evaluateService.deleteById(id);
            if(success){
                return ResponseMessage.ok();
            }
            return ResponseMessage.error();
     }
    /**
     * 主键查询
     */
    @PostMapping("/queryById")
    @RespMessage
    public ResponseMessage queryById(Long id){
            EvaluateEntity evaluateEntity = evaluateService.queryById(id);
            if(evaluateEntity == null){
                return ResponseMessage.ok().data(evaluateEntity);
            }
            return ResponseMessage.error().message("数据不存在");
     }
    /**
     * 通过主键集合查询
     */
    @PostMapping("/getList")
    @RespMessage
    public ResponseMessage getList(String ids){
            String[] idArr = ids.split(",");
            List list = Arrays.asList(idArr);
            List<EvaluateEntity> resultList = evaluateService.selectByIds(list);
            return ResponseMessage.ok().data(resultList);

     }
    /**
     * 分页查询
     */
    @PostMapping("/pageList")
    @RespMessage
    public ResponseMessage pageList(Page<EvaluateEntity> page,@RequestBody EvaluateEntity queryParam){
            QueryWrapper<EvaluateEntity> wrapper = new QueryWrapper(queryParam);
            page = evaluateService.queryPageList(page,wrapper);
            return ResponseMessage.ok().data(page);

     }
    /**
     * 查询全部
     */
    @PostMapping("/getAll")
    @RespMessage
    public List<EvaluateEntity> getAll(){
        return  evaluateService.list();
     }
    /**
     * 导出全部
     */
    @GetMapping("/exportAll")
    @RespMessage
    public void exportAll(HttpServletResponse response){
        List<EvaluateEntity> list = evaluateService.list();
        ExportParams exportParams = new ExportParams();
        exportParams.setSheetName("Evaluate");
        Workbook workbook = ExcelExportUtil.exportExcel(exportParams, EvaluateEntity.class, list);
        try {
            FileUtil.output(workbook,response,"Evaluate.xlsx");
        } catch (IOException e) {
            e.printStackTrace();
        }
     }
    /**
     * 导入
     */
    @PostMapping("/imports")
    @RespMessage
    public ResponseMessage imports(MultipartFile file){
        ImportParams importParams = new ImportParams();
        importParams.setTitleRows(0);
        importParams.setHeadRows(1);
        importParams.setSheetNum(1);
         try {
            List<EvaluateEntity> list = ExcelImportUtil.importExcel(file.getInputStream(),EvaluateEntity.class,importParams);
            boolean success = evaluateService.insertBatch(list);
            if(success){
                return ResponseMessage.ok();
            }
         } catch (Exception e) {

         }
         return ResponseMessage.error();
     }


}
