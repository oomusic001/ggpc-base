package com.ggpc.base.controller;

import com.ggpc.base.service.CarBrandService;
import com.ggpc.base.entity.CarBrandEntity;
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import java.io.IOException;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.GetMapping;
import org.apache.poi.ss.usermodel.Workbook;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import com.ggpc.base.web.advice.RespMessage;
import com.ggpc.base.web.advice.ResponseMessage;
import com.ggpc.base.web.util.FileUtil;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.Arrays;
import java.util.List;
import org.springframework.web.bind.annotation.RestController;

/**
 *  控制层
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@RestController
@RequestMapping("/carBrand")
public class CarBrandController {

    @Autowired
    private CarBrandService   carBrandService;
    /**
     * 新增
     */
     @PostMapping("/save")
     @RespMessage
     public ResponseMessage save(@RequestBody CarBrandEntity carBrandEntity){
            boolean success = carBrandService.save(carBrandEntity);
            if(success){
                return ResponseMessage.ok();
            }
            return ResponseMessage.error();
     }
    /**
     * 修改
     */
    @PostMapping("/update")
    @RespMessage
    public ResponseMessage update(@RequestBody CarBrandEntity carBrandEntity){
            boolean success = carBrandService.updateByPK(carBrandEntity);
            if(success){
                return ResponseMessage.ok();
            }
            return ResponseMessage.error().message("数据未更新或数据不存在");
     }
    /**
     * 删除
     */
    @PostMapping("/delete")
    @RespMessage
    public ResponseMessage delete(Long id){
            boolean success = carBrandService.deleteById(id);
            if(success){
                return ResponseMessage.ok();
            }
            return ResponseMessage.error();
     }
    /**
     * 主键查询
     */
    @PostMapping("/queryById")
    @RespMessage
    public ResponseMessage queryById(Long id){
            CarBrandEntity carBrandEntity = carBrandService.queryById(id);
            if(carBrandEntity == null){
                return ResponseMessage.ok().data(carBrandEntity);
            }
            return ResponseMessage.error().message("数据不存在");
     }
    /**
     * 通过主键集合查询
     */
    @PostMapping("/getList")
    @RespMessage
    public ResponseMessage getList(String ids){
            String[] idArr = ids.split(",");
            List list = Arrays.asList(idArr);
            List<CarBrandEntity> resultList = carBrandService.selectByIds(list);
            return ResponseMessage.ok().data(resultList);

     }
    /**
     * 分页查询
     */
    @PostMapping("/pageList")
    @RespMessage
    public ResponseMessage pageList(Page<CarBrandEntity> page,@RequestBody CarBrandEntity queryParam){
            QueryWrapper<CarBrandEntity> wrapper = new QueryWrapper(queryParam);
            page = carBrandService.queryPageList(page,wrapper);
            return ResponseMessage.ok().data(page);

     }
    /**
     * 查询全部
     */
    @PostMapping("/getAll")
    @RespMessage
    public List<CarBrandEntity> getAll(){
        return  carBrandService.list();
     }
    /**
     * 导出全部
     */
    @GetMapping("/exportAll")
    @RespMessage
    public void exportAll(HttpServletResponse response){
        List<CarBrandEntity> list = carBrandService.list();
        ExportParams exportParams = new ExportParams();
        exportParams.setSheetName("CarBrand");
        Workbook workbook = ExcelExportUtil.exportExcel(exportParams, CarBrandEntity.class, list);
        try {
            FileUtil.output(workbook,response,"CarBrand.xlsx");
        } catch (IOException e) {
            e.printStackTrace();
        }
     }
    /**
     * 导入
     */
    @PostMapping("/imports")
    @RespMessage
    public ResponseMessage imports(MultipartFile file){
        ImportParams importParams = new ImportParams();
        importParams.setTitleRows(0);
        importParams.setHeadRows(1);
        importParams.setSheetNum(1);
         try {
            List<CarBrandEntity> list = ExcelImportUtil.importExcel(file.getInputStream(),CarBrandEntity.class,importParams);
            boolean success = carBrandService.insertBatch(list);
            if(success){
                return ResponseMessage.ok();
            }
         } catch (Exception e) {

         }
         return ResponseMessage.error();
     }


}
