package com.ggpc.base.web.advice;



public class ResponseMessage {
    private Object data;
    private Integer code;
    private String message;
    private long timeStamp;
    private boolean success;
    private long useTime;
    private String errorMessage;
    public ResponseMessage useTime(long time){
        this.useTime = time;
        return this;
    }
    public static ResponseMessage ok(){
        return new ResponseMessage().setSuccess(true).setCode(200).message("操作成功");
    }
    public static ResponseMessage error(){
        return new ResponseMessage().setSuccess(false).setCode(500).message("操作失败");
    }
    /**
     * 将打印的异常堆栈信息返回给前端。方便开发调试。
     * 但是不要返回业务敏感信息。
     * 如登陆时不要提示：密码错误，用户名错误
     * 而是要通过message提示：用户名或密码错误。
     * @param errorMessage
     * @return
     */
    public ResponseMessage errMessage(String errorMessage){
        this.message = "操作失败";
        this.errorMessage = errorMessage;
        return this;
    }
    public ResponseMessage data(Object data){
        this.data = data;
        return this;
    }
    public ResponseMessage code(Integer code){
        this.code = code;
        return this;
    }
    public ResponseMessage message(String message){
        this.message = message;
        return this;
    }

    public ResponseMessage success(boolean success){
        this.success = success;
        return this;
    }

    public Integer getCode() {
        return code;
    }

    public ResponseMessage setCode(Integer code) {
        this.code = code;
        return this;
    }

    public String getMessage() {
        return message;
    }

    public ResponseMessage setMessage(String message) {
        this.message = message;
        return this;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

    public ResponseMessage setTimeStamp(long timeStamp) {
        this.timeStamp = timeStamp;
        return this;
    }

    public boolean isSuccess() {
        return success;
    }

    public ResponseMessage setSuccess(boolean success) {
        this.success = success;
        this.timeStamp = System.currentTimeMillis();
        return this;
    }


    public long getUseTime() {
        return useTime;
    }

    public ResponseMessage setUseTime(long useTime) {
        this.useTime = useTime;
        return this;
    }

    public Object getData() {
        return data;
    }

    public ResponseMessage setData(Object data) {
        this.data = data;
        return this;
    }

    public String getErrorMessage() {
        return errorMessage;
    }

    public void setErrorMessage(String errorMessage) {
        this.errorMessage = errorMessage;
    }
}
