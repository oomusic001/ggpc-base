package com.ggpc.base.web.util;

public class WebUtil {
    private long startTime ;
    private long endTime;
    private long useTime;
    static ThreadLocal<Long> threadLocal = new ThreadLocal<Long>();
    static ThreadLocal<Long> webThreadLocal = new ThreadLocal<>();

    public static void webStart(){
        webThreadLocal.set(System.currentTimeMillis());
    }
    public static long webUseTime(){
        return System.currentTimeMillis() - (webThreadLocal.get() == null ? System.currentTimeMillis() : webThreadLocal.get());
    }
    public static void remove(){
        webThreadLocal.remove();
    }
    /**
     * 将开始时间记录再私有线程中
     */
    public static void startLocal(){
        threadLocal.set(System.currentTimeMillis());
    }

    /**
     * 统计耗时
     * @return
     */
    public static long useTimeLocal(){
        long uTime = System.currentTimeMillis() - ((threadLocal.get() == null) ? System.currentTimeMillis() : threadLocal.get());
        threadLocal.remove();
        return uTime;
    }

    /**
     * 开始计时
     * @return
     */
    public static WebUtil start(){
        return new WebUtil().setStartTime(System.currentTimeMillis());
    }

    /**
     * 结束计时
     */
    private void end(){
        this.endTime = System.currentTimeMillis();
    }

    /**
     * 获取耗时时间
     * @return
     */
    public long useTime(){
        this.endTime = System.currentTimeMillis();
        this.useTime = this.endTime - this.startTime;
        return useTime;
    }

    public WebUtil setStartTime(long startTime) {
        this.startTime = startTime;
        return this;
    }

}
