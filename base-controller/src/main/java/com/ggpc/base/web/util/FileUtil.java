package com.ggpc.base.web.util;

import org.apache.poi.ss.usermodel.Workbook;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.OutputStream;

public class FileUtil {

    public static void output(Workbook workbook, HttpServletResponse response, String fileName) throws IOException {
        OutputStream output = response.getOutputStream();
        response.setContentType("application/vnd.ms-excel;charset=utf-8");
        response.setCharacterEncoding("utf-8");
        //path = URLDecoder.decode(resource.getPath(), "utf-8");
        fileName = new String(fileName.getBytes("utf-8"), "ISO8859-1");
        response.setHeader("Content-disposition", "attachment;filename=" + fileName);
        workbook.write(output);
        output.flush();
        output.close();

    }
}
