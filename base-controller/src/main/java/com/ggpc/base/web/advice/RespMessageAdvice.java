package com.ggpc.base.web.advice;



import com.ggpc.base.web.util.WebUtil;
import org.springframework.core.MethodParameter;
import org.springframework.http.MediaType;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.stereotype.Component;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.servlet.mvc.method.annotation.ResponseBodyAdvice;

@ControllerAdvice
@Component
public class RespMessageAdvice implements ResponseBodyAdvice {
    @Override
    public boolean supports(MethodParameter methodParameter, Class aClass) {
        return  methodParameter.hasMethodAnnotation(RespMessage.class);
    }

    /**
     * 加了 @RespMessage 注解的才会被拦截
     * @param result
     * @param methodParameter
     * @param mediaType
     * @param aClass
     * @param serverHttpRequest
     * @param serverHttpResponse
     * @return
     */
    @Override
    public Object beforeBodyWrite(Object result, MethodParameter methodParameter,
                                  MediaType mediaType, Class aClass,
                                  ServerHttpRequest serverHttpRequest,
                                  ServerHttpResponse serverHttpResponse) {
        RespMessage methodAnnotation = methodParameter.getMethodAnnotation(RespMessage.class);
        //获取接口耗时
        long webUseTime = WebUtil.webUseTime();
        //正常情况下methodAnnotation不会为空
        if (methodAnnotation != null){
            if (result instanceof ResponseMessage){
                return ((ResponseMessage)result).useTime(webUseTime);
            }
            return ResponseMessage.ok().data(result).useTime(webUseTime).message("操作成功");
        }
        return ResponseMessage.error().useTime(webUseTime).data(result).message("操作有误");
    }
}
