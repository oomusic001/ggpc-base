package com.ggpc.base.service;

import com.ggpc.base.entity.RoleEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.io.Serializable;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 *  服务类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
public interface RoleService extends IService<RoleEntity> {
        /**
         *  新增
         */
        public boolean insert(RoleEntity param);
        /**
         *  删除
         */
        public boolean deleteById(Long id);
        /**
         *  更新
         */
        public boolean updateByPK(RoleEntity param);
        /**
         *  查询
         */
        public RoleEntity queryById(Long id);
        /**
         *  根据id查询多条数据
         */
        public List<RoleEntity> selectByIds(List<Serializable> idList);

        /**
         *  分页查询
         */
        public Page<RoleEntity> queryPageList(Page<RoleEntity> page,QueryWrapper<RoleEntity> wrapper);

        /**
         *  通过条件查询列表
         */
        public List<RoleEntity>  selectListByCondition(QueryWrapper<RoleEntity> wrapper);
        /**
         *  批量保存
         */
        public boolean insertBatch(List<RoleEntity> list);
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(RoleEntity entity);
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<RoleEntity> list);
}
