package com.ggpc.base.service.impl;

import com.ggpc.base.entity.RoleEntity;
import com.ggpc.base.dao.RoleDao;
import com.ggpc.base.service.RoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import java.io.Serializable;


/**
 *  服务实现类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleDao, RoleEntity> implements RoleService {

        private static final Logger logger = LoggerFactory.getLogger(RoleServiceImpl.class);

        @Autowired
        private RoleDao roleDao;


        /**
         *  新增
         */
        public boolean insert(RoleEntity param){
             return save(param);
        }
        /**
         *  删除
         */
        public boolean deleteById(Long id){
             return roleDao.deleteById(id) == 1 ? true : false;
        }
        /**
         *  更新
         */
        public boolean updateByPK(RoleEntity param){
             return roleDao.updateById(param) == 1 ? true : false;
        }
        /**
         *  查询
         */
        public RoleEntity queryById(Long id){
             return roleDao.selectById(id);
        }
        /**
         *  根据id查询多条数据
         */
        public List<RoleEntity> selectByIds(List<Serializable> idList){
             return roleDao.selectBatchIds(idList);
        }
         /**
         *  条件分页查询
         */
        public Page<RoleEntity> queryPageList(Page<RoleEntity> page,QueryWrapper<RoleEntity> wrapper){
            return roleDao.selectPage(page,wrapper);
        }
        /**
         *  通过条件查询列表
         */
        public List<RoleEntity>  selectListByCondition(QueryWrapper<RoleEntity> wrapper){
            return roleDao.selectList(wrapper);
        }
        /**
         *  批量保存
         */
        public boolean insertBatch(List<RoleEntity> list){
            return saveBatch(list);
        }
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(RoleEntity entity){
                return saveOrUpdate(entity);
        }
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<RoleEntity> list){
            return saveOrUpdateBatch(list);
        }
}
