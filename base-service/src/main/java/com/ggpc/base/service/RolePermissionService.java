package com.ggpc.base.service;

import com.ggpc.base.entity.RolePermissionEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.io.Serializable;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 *  服务类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
public interface RolePermissionService extends IService<RolePermissionEntity> {
        /**
         *  新增
         */
        public boolean insert(RolePermissionEntity param);
        /**
         *  删除
         */
        public boolean deleteById(Long id);
        /**
         *  更新
         */
        public boolean updateByPK(RolePermissionEntity param);
        /**
         *  查询
         */
        public RolePermissionEntity queryById(Long id);
        /**
         *  根据id查询多条数据
         */
        public List<RolePermissionEntity> selectByIds(List<Serializable> idList);

        /**
         *  分页查询
         */
        public Page<RolePermissionEntity> queryPageList(Page<RolePermissionEntity> page,QueryWrapper<RolePermissionEntity> wrapper);

        /**
         *  通过条件查询列表
         */
        public List<RolePermissionEntity>  selectListByCondition(QueryWrapper<RolePermissionEntity> wrapper);
        /**
         *  批量保存
         */
        public boolean insertBatch(List<RolePermissionEntity> list);
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(RolePermissionEntity entity);
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<RolePermissionEntity> list);
}
