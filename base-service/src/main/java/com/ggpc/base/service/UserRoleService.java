package com.ggpc.base.service;

import com.ggpc.base.entity.UserRoleEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.io.Serializable;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 *  服务类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
public interface UserRoleService extends IService<UserRoleEntity> {
        /**
         *  新增
         */
        public boolean insert(UserRoleEntity param);
        /**
         *  删除
         */
        public boolean deleteById(Long id);
        /**
         *  更新
         */
        public boolean updateByPK(UserRoleEntity param);
        /**
         *  查询
         */
        public UserRoleEntity queryById(Long id);
        /**
         *  根据id查询多条数据
         */
        public List<UserRoleEntity> selectByIds(List<Serializable> idList);

        /**
         *  分页查询
         */
        public Page<UserRoleEntity> queryPageList(Page<UserRoleEntity> page,QueryWrapper<UserRoleEntity> wrapper);

        /**
         *  通过条件查询列表
         */
        public List<UserRoleEntity>  selectListByCondition(QueryWrapper<UserRoleEntity> wrapper);
        /**
         *  批量保存
         */
        public boolean insertBatch(List<UserRoleEntity> list);
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(UserRoleEntity entity);
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<UserRoleEntity> list);
}
