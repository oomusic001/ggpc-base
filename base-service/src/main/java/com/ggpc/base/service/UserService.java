package com.ggpc.base.service;

import com.ggpc.base.entity.UserEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.io.Serializable;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 *  服务类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
public interface UserService extends IService<UserEntity> {
        /**
         *  新增
         */
        public boolean insert(UserEntity param);
        /**
         *  删除
         */
        public boolean deleteById(Long id);
        /**
         *  更新
         */
        public boolean updateByPK(UserEntity param);
        /**
         *  查询
         */
        public UserEntity queryById(Long id);
        /**
         *  根据id查询多条数据
         */
        public List<UserEntity> selectByIds(List<Serializable> idList);

        /**
         *  分页查询
         */
        public Page<UserEntity> queryPageList(Page<UserEntity> page,QueryWrapper<UserEntity> wrapper);

        /**
         *  通过条件查询列表
         */
        public List<UserEntity>  selectListByCondition(QueryWrapper<UserEntity> wrapper);
        /**
         *  批量保存
         */
        public boolean insertBatch(List<UserEntity> list);
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(UserEntity entity);
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<UserEntity> list);
}
