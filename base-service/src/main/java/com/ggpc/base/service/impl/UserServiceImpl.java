package com.ggpc.base.service.impl;

import com.ggpc.base.entity.UserEntity;
import com.ggpc.base.dao.UserDao;
import com.ggpc.base.service.UserService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import java.io.Serializable;


/**
 *  服务实现类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@Service
public class UserServiceImpl extends ServiceImpl<UserDao, UserEntity> implements UserService {

        private static final Logger logger = LoggerFactory.getLogger(UserServiceImpl.class);

        @Autowired
        private UserDao userDao;


        /**
         *  新增
         */
        public boolean insert(UserEntity param){
             return save(param);
        }
        /**
         *  删除
         */
        public boolean deleteById(Long id){
             return userDao.deleteById(id) == 1 ? true : false;
        }
        /**
         *  更新
         */
        public boolean updateByPK(UserEntity param){
             return userDao.updateById(param) == 1 ? true : false;
        }
        /**
         *  查询
         */
        public UserEntity queryById(Long id){
             return userDao.selectById(id);
        }
        /**
         *  根据id查询多条数据
         */
        public List<UserEntity> selectByIds(List<Serializable> idList){
             return userDao.selectBatchIds(idList);
        }
         /**
         *  条件分页查询
         */
        public Page<UserEntity> queryPageList(Page<UserEntity> page,QueryWrapper<UserEntity> wrapper){
            return userDao.selectPage(page,wrapper);
        }
        /**
         *  通过条件查询列表
         */
        public List<UserEntity>  selectListByCondition(QueryWrapper<UserEntity> wrapper){
            return userDao.selectList(wrapper);
        }
        /**
         *  批量保存
         */
        public boolean insertBatch(List<UserEntity> list){
            return saveBatch(list);
        }
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(UserEntity entity){
                return saveOrUpdate(entity);
        }
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<UserEntity> list){
            return saveOrUpdateBatch(list);
        }
}
