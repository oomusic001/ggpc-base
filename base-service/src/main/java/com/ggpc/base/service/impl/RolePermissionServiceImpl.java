package com.ggpc.base.service.impl;

import com.ggpc.base.entity.RolePermissionEntity;
import com.ggpc.base.dao.RolePermissionDao;
import com.ggpc.base.service.RolePermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import java.io.Serializable;


/**
 *  服务实现类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@Service
public class RolePermissionServiceImpl extends ServiceImpl<RolePermissionDao, RolePermissionEntity> implements RolePermissionService {

        private static final Logger logger = LoggerFactory.getLogger(RolePermissionServiceImpl.class);

        @Autowired
        private RolePermissionDao rolePermissionDao;


        /**
         *  新增
         */
        public boolean insert(RolePermissionEntity param){
             return save(param);
        }
        /**
         *  删除
         */
        public boolean deleteById(Long id){
             return rolePermissionDao.deleteById(id) == 1 ? true : false;
        }
        /**
         *  更新
         */
        public boolean updateByPK(RolePermissionEntity param){
             return rolePermissionDao.updateById(param) == 1 ? true : false;
        }
        /**
         *  查询
         */
        public RolePermissionEntity queryById(Long id){
             return rolePermissionDao.selectById(id);
        }
        /**
         *  根据id查询多条数据
         */
        public List<RolePermissionEntity> selectByIds(List<Serializable> idList){
             return rolePermissionDao.selectBatchIds(idList);
        }
         /**
         *  条件分页查询
         */
        public Page<RolePermissionEntity> queryPageList(Page<RolePermissionEntity> page,QueryWrapper<RolePermissionEntity> wrapper){
            return rolePermissionDao.selectPage(page,wrapper);
        }
        /**
         *  通过条件查询列表
         */
        public List<RolePermissionEntity>  selectListByCondition(QueryWrapper<RolePermissionEntity> wrapper){
            return rolePermissionDao.selectList(wrapper);
        }
        /**
         *  批量保存
         */
        public boolean insertBatch(List<RolePermissionEntity> list){
            return saveBatch(list);
        }
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(RolePermissionEntity entity){
                return saveOrUpdate(entity);
        }
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<RolePermissionEntity> list){
            return saveOrUpdateBatch(list);
        }
}
