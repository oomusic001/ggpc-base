package com.ggpc.base.service.impl;

import com.ggpc.base.entity.PermissionEntity;
import com.ggpc.base.dao.PermissionDao;
import com.ggpc.base.service.PermissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import java.io.Serializable;


/**
 *  服务实现类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@Service
public class PermissionServiceImpl extends ServiceImpl<PermissionDao, PermissionEntity> implements PermissionService {

        private static final Logger logger = LoggerFactory.getLogger(PermissionServiceImpl.class);

        @Autowired
        private PermissionDao permissionDao;


        /**
         *  新增
         */
        public boolean insert(PermissionEntity param){
             return save(param);
        }
        /**
         *  删除
         */
        public boolean deleteById(Long id){
             return permissionDao.deleteById(id) == 1 ? true : false;
        }
        /**
         *  更新
         */
        public boolean updateByPK(PermissionEntity param){
             return permissionDao.updateById(param) == 1 ? true : false;
        }
        /**
         *  查询
         */
        public PermissionEntity queryById(Long id){
             return permissionDao.selectById(id);
        }
        /**
         *  根据id查询多条数据
         */
        public List<PermissionEntity> selectByIds(List<Serializable> idList){
             return permissionDao.selectBatchIds(idList);
        }
         /**
         *  条件分页查询
         */
        public Page<PermissionEntity> queryPageList(Page<PermissionEntity> page,QueryWrapper<PermissionEntity> wrapper){
            return permissionDao.selectPage(page,wrapper);
        }
        /**
         *  通过条件查询列表
         */
        public List<PermissionEntity>  selectListByCondition(QueryWrapper<PermissionEntity> wrapper){
            return permissionDao.selectList(wrapper);
        }
        /**
         *  批量保存
         */
        public boolean insertBatch(List<PermissionEntity> list){
            return saveBatch(list);
        }
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(PermissionEntity entity){
                return saveOrUpdate(entity);
        }
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<PermissionEntity> list){
            return saveOrUpdateBatch(list);
        }
}
