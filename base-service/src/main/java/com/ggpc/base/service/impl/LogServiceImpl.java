package com.ggpc.base.service.impl;

import com.ggpc.base.entity.LogEntity;
import com.ggpc.base.dao.LogDao;
import com.ggpc.base.service.LogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import java.io.Serializable;


/**
 *  服务实现类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@Service
public class LogServiceImpl extends ServiceImpl<LogDao, LogEntity> implements LogService {

        private static final Logger logger = LoggerFactory.getLogger(LogServiceImpl.class);

        @Autowired
        private LogDao logDao;


        /**
         *  新增
         */
        public boolean insert(LogEntity param){
             return save(param);
        }
        /**
         *  删除
         */
        public boolean deleteById(Long id){
             return logDao.deleteById(id) == 1 ? true : false;
        }
        /**
         *  更新
         */
        public boolean updateByPK(LogEntity param){
             return logDao.updateById(param) == 1 ? true : false;
        }
        /**
         *  查询
         */
        public LogEntity queryById(Long id){
             return logDao.selectById(id);
        }
        /**
         *  根据id查询多条数据
         */
        public List<LogEntity> selectByIds(List<Serializable> idList){
             return logDao.selectBatchIds(idList);
        }
         /**
         *  条件分页查询
         */
        public Page<LogEntity> queryPageList(Page<LogEntity> page,QueryWrapper<LogEntity> wrapper){
            return logDao.selectPage(page,wrapper);
        }
        /**
         *  通过条件查询列表
         */
        public List<LogEntity>  selectListByCondition(QueryWrapper<LogEntity> wrapper){
            return logDao.selectList(wrapper);
        }
        /**
         *  批量保存
         */
        public boolean insertBatch(List<LogEntity> list){
            return saveBatch(list);
        }
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(LogEntity entity){
                return saveOrUpdate(entity);
        }
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<LogEntity> list){
            return saveOrUpdateBatch(list);
        }
}
