package com.ggpc.base.service;

import com.ggpc.base.entity.AreaEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.io.Serializable;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 *  服务类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
public interface AreaService extends IService<AreaEntity> {
        /**
         *  新增
         */
        public boolean insert(AreaEntity param);
        /**
         *  删除
         */
        public boolean deleteById(Long id);
        /**
         *  更新
         */
        public boolean updateByPK(AreaEntity param);
        /**
         *  查询
         */
        public AreaEntity queryById(Long id);
        /**
         *  根据id查询多条数据
         */
        public List<AreaEntity> selectByIds(List<Serializable> idList);

        /**
         *  分页查询
         */
        public Page<AreaEntity> queryPageList(Page<AreaEntity> page,QueryWrapper<AreaEntity> wrapper);

        /**
         *  通过条件查询列表
         */
        public List<AreaEntity>  selectListByCondition(QueryWrapper<AreaEntity> wrapper);
        /**
         *  批量保存
         */
        public boolean insertBatch(List<AreaEntity> list);
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(AreaEntity entity);
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<AreaEntity> list);
}
