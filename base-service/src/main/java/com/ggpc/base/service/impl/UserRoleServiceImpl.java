package com.ggpc.base.service.impl;

import com.ggpc.base.entity.UserRoleEntity;
import com.ggpc.base.dao.UserRoleDao;
import com.ggpc.base.service.UserRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import java.io.Serializable;


/**
 *  服务实现类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@Service
public class UserRoleServiceImpl extends ServiceImpl<UserRoleDao, UserRoleEntity> implements UserRoleService {

        private static final Logger logger = LoggerFactory.getLogger(UserRoleServiceImpl.class);

        @Autowired
        private UserRoleDao userRoleDao;


        /**
         *  新增
         */
        public boolean insert(UserRoleEntity param){
             return save(param);
        }
        /**
         *  删除
         */
        public boolean deleteById(Long id){
             return userRoleDao.deleteById(id) == 1 ? true : false;
        }
        /**
         *  更新
         */
        public boolean updateByPK(UserRoleEntity param){
             return userRoleDao.updateById(param) == 1 ? true : false;
        }
        /**
         *  查询
         */
        public UserRoleEntity queryById(Long id){
             return userRoleDao.selectById(id);
        }
        /**
         *  根据id查询多条数据
         */
        public List<UserRoleEntity> selectByIds(List<Serializable> idList){
             return userRoleDao.selectBatchIds(idList);
        }
         /**
         *  条件分页查询
         */
        public Page<UserRoleEntity> queryPageList(Page<UserRoleEntity> page,QueryWrapper<UserRoleEntity> wrapper){
            return userRoleDao.selectPage(page,wrapper);
        }
        /**
         *  通过条件查询列表
         */
        public List<UserRoleEntity>  selectListByCondition(QueryWrapper<UserRoleEntity> wrapper){
            return userRoleDao.selectList(wrapper);
        }
        /**
         *  批量保存
         */
        public boolean insertBatch(List<UserRoleEntity> list){
            return saveBatch(list);
        }
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(UserRoleEntity entity){
                return saveOrUpdate(entity);
        }
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<UserRoleEntity> list){
            return saveOrUpdateBatch(list);
        }
}
