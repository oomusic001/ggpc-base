package com.ggpc.base.service.impl;

import com.ggpc.base.entity.OrdersEntity;
import com.ggpc.base.dao.OrdersDao;
import com.ggpc.base.service.OrdersService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import java.io.Serializable;


/**
 *  服务实现类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@Service
public class OrdersServiceImpl extends ServiceImpl<OrdersDao, OrdersEntity> implements OrdersService {

        private static final Logger logger = LoggerFactory.getLogger(OrdersServiceImpl.class);

        @Autowired
        private OrdersDao ordersDao;


        /**
         *  新增
         */
        public boolean insert(OrdersEntity param){
             return save(param);
        }
        /**
         *  删除
         */
        public boolean deleteById(Long id){
             return ordersDao.deleteById(id) == 1 ? true : false;
        }
        /**
         *  更新
         */
        public boolean updateByPK(OrdersEntity param){
             return ordersDao.updateById(param) == 1 ? true : false;
        }
        /**
         *  查询
         */
        public OrdersEntity queryById(Long id){
             return ordersDao.selectById(id);
        }
        /**
         *  根据id查询多条数据
         */
        public List<OrdersEntity> selectByIds(List<Serializable> idList){
             return ordersDao.selectBatchIds(idList);
        }
         /**
         *  条件分页查询
         */
        public Page<OrdersEntity> queryPageList(Page<OrdersEntity> page,QueryWrapper<OrdersEntity> wrapper){
            return ordersDao.selectPage(page,wrapper);
        }
        /**
         *  通过条件查询列表
         */
        public List<OrdersEntity>  selectListByCondition(QueryWrapper<OrdersEntity> wrapper){
            return ordersDao.selectList(wrapper);
        }
        /**
         *  批量保存
         */
        public boolean insertBatch(List<OrdersEntity> list){
            return saveBatch(list);
        }
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(OrdersEntity entity){
                return saveOrUpdate(entity);
        }
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<OrdersEntity> list){
            return saveOrUpdateBatch(list);
        }
}
