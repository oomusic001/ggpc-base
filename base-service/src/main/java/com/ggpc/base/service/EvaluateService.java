package com.ggpc.base.service;

import com.ggpc.base.entity.EvaluateEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.io.Serializable;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 *  服务类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
public interface EvaluateService extends IService<EvaluateEntity> {
        /**
         *  新增
         */
        public boolean insert(EvaluateEntity param);
        /**
         *  删除
         */
        public boolean deleteById(Long id);
        /**
         *  更新
         */
        public boolean updateByPK(EvaluateEntity param);
        /**
         *  查询
         */
        public EvaluateEntity queryById(Long id);
        /**
         *  根据id查询多条数据
         */
        public List<EvaluateEntity> selectByIds(List<Serializable> idList);

        /**
         *  分页查询
         */
        public Page<EvaluateEntity> queryPageList(Page<EvaluateEntity> page,QueryWrapper<EvaluateEntity> wrapper);

        /**
         *  通过条件查询列表
         */
        public List<EvaluateEntity>  selectListByCondition(QueryWrapper<EvaluateEntity> wrapper);
        /**
         *  批量保存
         */
        public boolean insertBatch(List<EvaluateEntity> list);
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(EvaluateEntity entity);
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<EvaluateEntity> list);
}
