package com.ggpc.base.service.impl;

import com.ggpc.base.entity.EvaluateEntity;
import com.ggpc.base.dao.EvaluateDao;
import com.ggpc.base.service.EvaluateService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import java.io.Serializable;


/**
 *  服务实现类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@Service
public class EvaluateServiceImpl extends ServiceImpl<EvaluateDao, EvaluateEntity> implements EvaluateService {

        private static final Logger logger = LoggerFactory.getLogger(EvaluateServiceImpl.class);

        @Autowired
        private EvaluateDao evaluateDao;


        /**
         *  新增
         */
        public boolean insert(EvaluateEntity param){
             return save(param);
        }
        /**
         *  删除
         */
        public boolean deleteById(Long id){
             return evaluateDao.deleteById(id) == 1 ? true : false;
        }
        /**
         *  更新
         */
        public boolean updateByPK(EvaluateEntity param){
             return evaluateDao.updateById(param) == 1 ? true : false;
        }
        /**
         *  查询
         */
        public EvaluateEntity queryById(Long id){
             return evaluateDao.selectById(id);
        }
        /**
         *  根据id查询多条数据
         */
        public List<EvaluateEntity> selectByIds(List<Serializable> idList){
             return evaluateDao.selectBatchIds(idList);
        }
         /**
         *  条件分页查询
         */
        public Page<EvaluateEntity> queryPageList(Page<EvaluateEntity> page,QueryWrapper<EvaluateEntity> wrapper){
            return evaluateDao.selectPage(page,wrapper);
        }
        /**
         *  通过条件查询列表
         */
        public List<EvaluateEntity>  selectListByCondition(QueryWrapper<EvaluateEntity> wrapper){
            return evaluateDao.selectList(wrapper);
        }
        /**
         *  批量保存
         */
        public boolean insertBatch(List<EvaluateEntity> list){
            return saveBatch(list);
        }
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(EvaluateEntity entity){
                return saveOrUpdate(entity);
        }
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<EvaluateEntity> list){
            return saveOrUpdateBatch(list);
        }
}
