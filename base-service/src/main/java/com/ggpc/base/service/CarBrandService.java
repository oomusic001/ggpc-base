package com.ggpc.base.service;

import com.ggpc.base.entity.CarBrandEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.io.Serializable;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 *  服务类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
public interface CarBrandService extends IService<CarBrandEntity> {
        /**
         *  新增
         */
        public boolean insert(CarBrandEntity param);
        /**
         *  删除
         */
        public boolean deleteById(Long id);
        /**
         *  更新
         */
        public boolean updateByPK(CarBrandEntity param);
        /**
         *  查询
         */
        public CarBrandEntity queryById(Long id);
        /**
         *  根据id查询多条数据
         */
        public List<CarBrandEntity> selectByIds(List<Serializable> idList);

        /**
         *  分页查询
         */
        public Page<CarBrandEntity> queryPageList(Page<CarBrandEntity> page,QueryWrapper<CarBrandEntity> wrapper);

        /**
         *  通过条件查询列表
         */
        public List<CarBrandEntity>  selectListByCondition(QueryWrapper<CarBrandEntity> wrapper);
        /**
         *  批量保存
         */
        public boolean insertBatch(List<CarBrandEntity> list);
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(CarBrandEntity entity);
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<CarBrandEntity> list);
}
