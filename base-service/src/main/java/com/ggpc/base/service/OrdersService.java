package com.ggpc.base.service;

import com.ggpc.base.entity.OrdersEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.io.Serializable;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 *  服务类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
public interface OrdersService extends IService<OrdersEntity> {
        /**
         *  新增
         */
        public boolean insert(OrdersEntity param);
        /**
         *  删除
         */
        public boolean deleteById(Long id);
        /**
         *  更新
         */
        public boolean updateByPK(OrdersEntity param);
        /**
         *  查询
         */
        public OrdersEntity queryById(Long id);
        /**
         *  根据id查询多条数据
         */
        public List<OrdersEntity> selectByIds(List<Serializable> idList);

        /**
         *  分页查询
         */
        public Page<OrdersEntity> queryPageList(Page<OrdersEntity> page,QueryWrapper<OrdersEntity> wrapper);

        /**
         *  通过条件查询列表
         */
        public List<OrdersEntity>  selectListByCondition(QueryWrapper<OrdersEntity> wrapper);
        /**
         *  批量保存
         */
        public boolean insertBatch(List<OrdersEntity> list);
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(OrdersEntity entity);
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<OrdersEntity> list);
}
