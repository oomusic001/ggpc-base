package com.ggpc.base.service;

import com.ggpc.base.entity.LogEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.io.Serializable;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 *  服务类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
public interface LogService extends IService<LogEntity> {
        /**
         *  新增
         */
        public boolean insert(LogEntity param);
        /**
         *  删除
         */
        public boolean deleteById(Long id);
        /**
         *  更新
         */
        public boolean updateByPK(LogEntity param);
        /**
         *  查询
         */
        public LogEntity queryById(Long id);
        /**
         *  根据id查询多条数据
         */
        public List<LogEntity> selectByIds(List<Serializable> idList);

        /**
         *  分页查询
         */
        public Page<LogEntity> queryPageList(Page<LogEntity> page,QueryWrapper<LogEntity> wrapper);

        /**
         *  通过条件查询列表
         */
        public List<LogEntity>  selectListByCondition(QueryWrapper<LogEntity> wrapper);
        /**
         *  批量保存
         */
        public boolean insertBatch(List<LogEntity> list);
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(LogEntity entity);
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<LogEntity> list);
}
