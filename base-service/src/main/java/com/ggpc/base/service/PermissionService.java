package com.ggpc.base.service;

import com.ggpc.base.entity.PermissionEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.io.Serializable;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 *  服务类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
public interface PermissionService extends IService<PermissionEntity> {
        /**
         *  新增
         */
        public boolean insert(PermissionEntity param);
        /**
         *  删除
         */
        public boolean deleteById(Long id);
        /**
         *  更新
         */
        public boolean updateByPK(PermissionEntity param);
        /**
         *  查询
         */
        public PermissionEntity queryById(Long id);
        /**
         *  根据id查询多条数据
         */
        public List<PermissionEntity> selectByIds(List<Serializable> idList);

        /**
         *  分页查询
         */
        public Page<PermissionEntity> queryPageList(Page<PermissionEntity> page,QueryWrapper<PermissionEntity> wrapper);

        /**
         *  通过条件查询列表
         */
        public List<PermissionEntity>  selectListByCondition(QueryWrapper<PermissionEntity> wrapper);
        /**
         *  批量保存
         */
        public boolean insertBatch(List<PermissionEntity> list);
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(PermissionEntity entity);
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<PermissionEntity> list);
}
