package com.ggpc.base.service.impl;

import com.ggpc.base.entity.DictEntity;
import com.ggpc.base.dao.DictDao;
import com.ggpc.base.service.DictService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import java.io.Serializable;


/**
 *  服务实现类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@Service
public class DictServiceImpl extends ServiceImpl<DictDao, DictEntity> implements DictService {

        private static final Logger logger = LoggerFactory.getLogger(DictServiceImpl.class);

        @Autowired
        private DictDao dictDao;


        /**
         *  新增
         */
        public boolean insert(DictEntity param){
             return save(param);
        }
        /**
         *  删除
         */
        public boolean deleteById(Long id){
             return dictDao.deleteById(id) == 1 ? true : false;
        }
        /**
         *  更新
         */
        public boolean updateByPK(DictEntity param){
             return dictDao.updateById(param) == 1 ? true : false;
        }
        /**
         *  查询
         */
        public DictEntity queryById(Long id){
             return dictDao.selectById(id);
        }
        /**
         *  根据id查询多条数据
         */
        public List<DictEntity> selectByIds(List<Serializable> idList){
             return dictDao.selectBatchIds(idList);
        }
         /**
         *  条件分页查询
         */
        public Page<DictEntity> queryPageList(Page<DictEntity> page,QueryWrapper<DictEntity> wrapper){
            return dictDao.selectPage(page,wrapper);
        }
        /**
         *  通过条件查询列表
         */
        public List<DictEntity>  selectListByCondition(QueryWrapper<DictEntity> wrapper){
            return dictDao.selectList(wrapper);
        }
        /**
         *  批量保存
         */
        public boolean insertBatch(List<DictEntity> list){
            return saveBatch(list);
        }
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(DictEntity entity){
                return saveOrUpdate(entity);
        }
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<DictEntity> list){
            return saveOrUpdateBatch(list);
        }
}
