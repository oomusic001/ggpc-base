package com.ggpc.base.service.impl;

import com.ggpc.base.entity.AreaEntity;
import com.ggpc.base.dao.AreaDao;
import com.ggpc.base.service.AreaService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import java.io.Serializable;


/**
 *  服务实现类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@Service
public class AreaServiceImpl extends ServiceImpl<AreaDao, AreaEntity> implements AreaService {

        private static final Logger logger = LoggerFactory.getLogger(AreaServiceImpl.class);

        @Autowired
        private AreaDao areaDao;


        /**
         *  新增
         */
        public boolean insert(AreaEntity param){
             return save(param);
        }
        /**
         *  删除
         */
        public boolean deleteById(Long id){
             return areaDao.deleteById(id) == 1 ? true : false;
        }
        /**
         *  更新
         */
        public boolean updateByPK(AreaEntity param){
             return areaDao.updateById(param) == 1 ? true : false;
        }
        /**
         *  查询
         */
        public AreaEntity queryById(Long id){
             return areaDao.selectById(id);
        }
        /**
         *  根据id查询多条数据
         */
        public List<AreaEntity> selectByIds(List<Serializable> idList){
             return areaDao.selectBatchIds(idList);
        }
         /**
         *  条件分页查询
         */
        public Page<AreaEntity> queryPageList(Page<AreaEntity> page,QueryWrapper<AreaEntity> wrapper){
            return areaDao.selectPage(page,wrapper);
        }
        /**
         *  通过条件查询列表
         */
        public List<AreaEntity>  selectListByCondition(QueryWrapper<AreaEntity> wrapper){
            return areaDao.selectList(wrapper);
        }
        /**
         *  批量保存
         */
        public boolean insertBatch(List<AreaEntity> list){
            return saveBatch(list);
        }
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(AreaEntity entity){
                return saveOrUpdate(entity);
        }
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<AreaEntity> list){
            return saveOrUpdateBatch(list);
        }
}
