package com.ggpc.base.service.impl;

import com.ggpc.base.entity.CarBrandEntity;
import com.ggpc.base.dao.CarBrandDao;
import com.ggpc.base.service.CarBrandService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import java.io.Serializable;


/**
 *  服务实现类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
@Service
public class CarBrandServiceImpl extends ServiceImpl<CarBrandDao, CarBrandEntity> implements CarBrandService {

        private static final Logger logger = LoggerFactory.getLogger(CarBrandServiceImpl.class);

        @Autowired
        private CarBrandDao carBrandDao;


        /**
         *  新增
         */
        public boolean insert(CarBrandEntity param){
             return save(param);
        }
        /**
         *  删除
         */
        public boolean deleteById(Long id){
             return carBrandDao.deleteById(id) == 1 ? true : false;
        }
        /**
         *  更新
         */
        public boolean updateByPK(CarBrandEntity param){
             return carBrandDao.updateById(param) == 1 ? true : false;
        }
        /**
         *  查询
         */
        public CarBrandEntity queryById(Long id){
             return carBrandDao.selectById(id);
        }
        /**
         *  根据id查询多条数据
         */
        public List<CarBrandEntity> selectByIds(List<Serializable> idList){
             return carBrandDao.selectBatchIds(idList);
        }
         /**
         *  条件分页查询
         */
        public Page<CarBrandEntity> queryPageList(Page<CarBrandEntity> page,QueryWrapper<CarBrandEntity> wrapper){
            return carBrandDao.selectPage(page,wrapper);
        }
        /**
         *  通过条件查询列表
         */
        public List<CarBrandEntity>  selectListByCondition(QueryWrapper<CarBrandEntity> wrapper){
            return carBrandDao.selectList(wrapper);
        }
        /**
         *  批量保存
         */
        public boolean insertBatch(List<CarBrandEntity> list){
            return saveBatch(list);
        }
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(CarBrandEntity entity){
                return saveOrUpdate(entity);
        }
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<CarBrandEntity> list){
            return saveOrUpdateBatch(list);
        }
}
