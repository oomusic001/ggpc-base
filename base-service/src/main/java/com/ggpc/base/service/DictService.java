package com.ggpc.base.service;

import com.ggpc.base.entity.DictEntity;
import com.baomidou.mybatisplus.extension.service.IService;
import java.util.List;
import java.io.Serializable;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 *  服务类
 * @author gey8866@126.com
 * @since 2021-06-08
 */
public interface DictService extends IService<DictEntity> {
        /**
         *  新增
         */
        public boolean insert(DictEntity param);
        /**
         *  删除
         */
        public boolean deleteById(Long id);
        /**
         *  更新
         */
        public boolean updateByPK(DictEntity param);
        /**
         *  查询
         */
        public DictEntity queryById(Long id);
        /**
         *  根据id查询多条数据
         */
        public List<DictEntity> selectByIds(List<Serializable> idList);

        /**
         *  分页查询
         */
        public Page<DictEntity> queryPageList(Page<DictEntity> page,QueryWrapper<DictEntity> wrapper);

        /**
         *  通过条件查询列表
         */
        public List<DictEntity>  selectListByCondition(QueryWrapper<DictEntity> wrapper);
        /**
         *  批量保存
         */
        public boolean insertBatch(List<DictEntity> list);
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(DictEntity entity);
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<DictEntity> list);
}
