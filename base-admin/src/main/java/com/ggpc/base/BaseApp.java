package com.ggpc.base;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * Hello world!
 *
 */
@SpringBootApplication
@MapperScan("com.ggpc.base.dao")
public class BaseApp {
    public static void main( String[] args ) {
        SpringApplication.run(BaseApp.class,args);
    }
}
