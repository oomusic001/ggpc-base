package ${package.Service};

import ${package.Entity}.${entity};
import ${superServiceClassPackage};
import java.util.List;
import java.io.Serializable;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
/**
 * ${table.comment!} 服务类
 * @author ${author}
 * @since ${date}
 */
<#if kotlin>
interface ${table.serviceName} : ${superServiceClass}<${entity}>
<#else>
public interface ${table.serviceName} extends ${superServiceClass}<${entity}> {
        /**
         *  新增
         */
        public boolean insert(${entity} param);
        /**
         *  删除
         */
        public boolean deleteById(Long id);
        /**
         *  更新
         */
        public boolean updateByPK(${entity} param);
        /**
         *  查询
         */
        public ${entity} queryById(Long id);
        /**
         *  根据id查询多条数据
         */
        public List<${entity}> selectByIds(List<Serializable> idList);

        /**
         *  分页查询
         */
        public Page<${entity}> queryPageList(Page<${entity}> page,QueryWrapper<${entity}> wrapper);

        /**
         *  通过条件查询列表
         */
        public List<${entity}>  selectListByCondition(QueryWrapper<${entity}> wrapper);
        /**
         *  批量保存
         */
        public boolean insertBatch(List<${entity}> list);
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(${entity} entity);
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<${entity}> list);
}
</#if>
