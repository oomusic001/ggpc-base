package ${package.ServiceImpl};

import ${package.Entity}.${entity};
import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};
import ${superServiceImplClassPackage};
import org.springframework.stereotype.Service;
import java.util.List;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import java.io.Serializable;


/**
 * ${table.comment!} 服务实现类
 * @author ${author}
 * @since ${date}
 */
@Service
<#if kotlin>
open class ${table.serviceImplName} : ${superServiceImplClass}<${table.mapperName}, ${entity}>(), ${table.serviceName} {

}
<#else>
public class ${table.serviceImplName} extends ${superServiceImplClass}<${table.mapperName}, ${entity}> implements ${table.serviceName} {

        private static final Logger logger = LoggerFactory.getLogger(${table.serviceImplName}.class);

        @Autowired
        private ${table.mapperName} ${table.mapperName?uncap_first};


        /**
         *  新增
         */
        public boolean insert(${entity} param){
             return save(param);
        }
        /**
         *  删除
         */
        public boolean deleteById(Long id){
             return ${table.mapperName?uncap_first}.deleteById(id) == 1 ? true : false;
        }
        /**
         *  更新
         */
        public boolean updateByPK(${entity} param){
             return ${table.mapperName?uncap_first}.updateById(param) == 1 ? true : false;
        }
        /**
         *  查询
         */
        public ${entity} queryById(Long id){
             return ${table.mapperName?uncap_first}.selectById(id);
        }
        /**
         *  根据id查询多条数据
         */
        public List<${entity}> selectByIds(List<Serializable> idList){
             return ${table.mapperName?uncap_first}.selectBatchIds(idList);
        }
         /**
         *  条件分页查询
         */
        public Page<${entity}> queryPageList(Page<${entity}> page,QueryWrapper<${entity}> wrapper){
            return ${table.mapperName?uncap_first}.selectPage(page,wrapper);
        }
        /**
         *  通过条件查询列表
         */
        public List<${entity}>  selectListByCondition(QueryWrapper<${entity}> wrapper){
            return ${table.mapperName?uncap_first}.selectList(wrapper);
        }
        /**
         *  批量保存
         */
        public boolean insertBatch(List<${entity}> list){
            return saveBatch(list);
        }
        /**
         *  保存或新增单条
         */
        public boolean insertOrUpdate(${entity} entity){
                return saveOrUpdate(entity);
        }
        /**
         *  批量保存或新增
         */
        public boolean insertOrUpdateBatch(List<${entity}> list){
            return saveOrUpdateBatch(list);
        }
}
</#if>
