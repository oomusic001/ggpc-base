package com.ggpc.base.generator;

import com.baomidou.mybatisplus.core.toolkit.StringPool;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.InjectionConfig;
import com.baomidou.mybatisplus.generator.config.*;
import com.baomidou.mybatisplus.generator.config.converts.MySqlTypeConvert;
import com.baomidou.mybatisplus.generator.config.po.TableInfo;
import com.baomidou.mybatisplus.generator.config.rules.DbColumnType;
import com.baomidou.mybatisplus.generator.config.rules.IColumnType;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;
import java.util.Properties;

public class MybatisPlusGenerator {
    public static void main(String[] args) throws IOException {
        String projectPath = System.getProperty("user.dir");
        URL u =  MybatisPlusGenerator.class.getResource("/generator.properties");
        InputStream is = new BufferedInputStream(new FileInputStream(u.getPath()));
        Properties properties = new Properties();
        properties.load(is);
        //数据源
        String url = properties.getProperty("url");
        String driverName = properties.getProperty("driverName");
        String username = properties.getProperty("username");
        String password = properties.getProperty("password");
        //包
        String xmlMapperPackage = properties.getProperty("xmlMapperPackage");
        String daoMapperPackage = properties.getProperty("daoMapperPackage");
        String entityPackage = properties.getProperty("entityPackage");
        String servicePackage = properties.getProperty("servicePackage");
        String serviceImplPackage = properties.getProperty("serviceImplPackage");
        String controllerPackage = properties.getProperty("controllerPackage");
        //父类文件
        String supperEntityFile = properties.getProperty("supperEntityFile");
        String supperMapperFile = properties.getProperty("supperMapperFile");
        //文件路径
        String controllerPath = properties.getProperty("controllerPath");
        String servicePath = properties.getProperty("servicePath");
        String serviceImplPath = properties.getProperty("serviceImplPath");
        String xmlMapperPath = properties.getProperty("xmlMapperPath");
        String daoMapperPath = properties.getProperty("daoMapperPath");
        String entityPath = properties.getProperty("entityPath");
        //模板
        String mapperXmlTemplate = properties.getProperty("mapperXmlTemplate");
        String serviceTemplate = properties.getProperty("serviceTemplate");
        String serviceImplTemplate = properties.getProperty("serviceImplTemplate");
        String controllerTemplate = properties.getProperty("controllerTemplate");
        String entityTemplate = properties.getProperty("entityTemplate");
        String daoXmlTemplate = properties.getProperty("daoXmlTemplate");
        //前缀、后缀
        String entitySuffix = "Entity";
        String controllerSuffix = "Controller";
        String serviceSuffix = "Service";
        String serviceImplSuffix = "ServiceImpl";
        String xmlMapperSuffix = "Mapper";
        String daoMapperSuffix = "Dao";
        String prefix = "%s";
        String dot = ".";
        String author = properties.getProperty("author");
        String tableName = properties.getProperty("tableName");
        // 代码生成器
        AutoGenerator generator = new AutoGenerator();
        // 全局配置
        GlobalConfig globalConfig = new GlobalConfig();
        globalConfig.setOutputDir(projectPath + "/src/main/java");
        globalConfig.setAuthor(author);
        globalConfig.setOpen(false);//是否打开输出目录
        globalConfig.setEnableCache(false);//是否在xml中添加二级缓存配置
        globalConfig.setKotlin(false);//开启 Kotlin 模式
        globalConfig.setBaseResultMap(true);
        globalConfig.setBaseColumnList(true);
        globalConfig.setEntityName(prefix + entitySuffix);//entity.java
        globalConfig.setXmlName(prefix + xmlMapperSuffix);//xml.mapper
        globalConfig.setMapperName(prefix+daoMapperSuffix);//dao.java
        globalConfig.setControllerName(prefix + controllerSuffix);//controller.java
        globalConfig.setServiceName(prefix + serviceSuffix);//service.java
        globalConfig.setServiceImplName(prefix + serviceImplSuffix);//serviceImpl.java
        globalConfig.setFileOverride(true);// 是否覆盖已有文件
        generator.setGlobalConfig(globalConfig);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setUrl(url);
        //dsc.setSchemaName("public");
        dsc.setDriverName(driverName);
        dsc.setUsername(username);
        dsc.setPassword(password);
        dsc.setTypeConvert(new ITypeConvert() {
            @Override
            public IColumnType processTypeConvert(GlobalConfig globalConfig, String fieldType) {
                String t = fieldType.toLowerCase();
                if(t.contains("datetime") || t.contains("timestamp")){
                    return DbColumnType.DATE;
                }
                if (t.contains("varchar")){
                    return DbColumnType.STRING;
                }
                //其它字段采用默认转换（非mysql数据库可以使用其它默认的数据库转换器）
                return new MySqlTypeConvert().processTypeConvert(globalConfig,fieldType);
            }
        });
        generator.setDataSource(dsc);

        // 包配置
        PackageConfig pc = new PackageConfig();
       // pc.setModuleName("model");
        pc.setParent(null);
        // 这个地址是生成的配置文件的包路径
        //【输出】配置和下方的自定义输出配置等效
        pc.setEntity(entityPackage);
        pc.setService(servicePackage);
        pc.setServiceImpl(serviceImplPackage);
        pc.setController(controllerPackage);
        pc.setMapper(daoMapperPackage);
        //pc.setXml(xmlMapperPackage);
        generator.setPackageInfo(pc);
        // 自定义配置
        InjectionConfig selfConfig = new InjectionConfig() {
            @Override
            public void initMap() {
                // to do nothing
            }
        };
        //xml模板引擎
        // 自定义【输出】配置
        List<FileOutConfig> templateList = new ArrayList<>();
        // 自定义配置会被优先输出
        templateList.add(new FileOutConfig(mapperXmlTemplate) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                // 自定义输出文件名
                return projectPath + xmlMapperPath + tableInfo.getEntityName().replace(entitySuffix,xmlMapperSuffix) + StringPool.DOT_XML;
            }
        });
        // 调整 controller 生成目录演示
        templateList.add(new FileOutConfig(controllerTemplate) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                //输出的位置
                return projectPath + controllerPath + tableInfo.getEntityName().replace(entitySuffix,controllerSuffix) + StringPool.DOT_JAVA;
            }
        });
        templateList.add(new FileOutConfig(serviceTemplate) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                //输出的位置
                return projectPath + servicePath + tableInfo.getEntityName().replace(entitySuffix,serviceSuffix) + StringPool.DOT_JAVA;
            }
        });
        templateList.add(new FileOutConfig(serviceImplTemplate) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                //输出的位置
                return projectPath + serviceImplPath + tableInfo.getEntityName().replace(entitySuffix,serviceImplSuffix) + StringPool.DOT_JAVA;
            }
        });
        templateList.add(new FileOutConfig(daoXmlTemplate) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                //输出的位置
                return projectPath + daoMapperPath + tableInfo.getEntityName().replace(entitySuffix,daoMapperSuffix) +  StringPool.DOT_JAVA;
            }
        });
        templateList.add(new FileOutConfig(entityTemplate) {
            @Override
            public String outputFile(TableInfo tableInfo) {
                //输出的位置
                return projectPath + entityPath + tableInfo.getEntityName() +  StringPool.DOT_JAVA;
            }
        });
        selfConfig.setFileOutConfigList(templateList);
        generator.setCfg(selfConfig);
        // 配置模板//这里使用自定义的模板引擎，如不配置，将采用默认的。
        TemplateConfig templateConfig = new TemplateConfig();
        templateConfig.setEntity(entityTemplate.substring(0,entityTemplate.lastIndexOf(dot)));
        templateConfig.setService(serviceTemplate.substring(0,serviceTemplate.lastIndexOf(dot)));
        templateConfig.setController(controllerTemplate.substring(0,controllerTemplate.lastIndexOf(dot)));
        templateConfig.setServiceImpl(serviceImplTemplate.substring(0,serviceImplTemplate.lastIndexOf(dot)));
        templateConfig.setXml(mapperXmlTemplate.substring(0,mapperXmlTemplate.lastIndexOf(dot)));
        templateConfig.setMapper(daoXmlTemplate.substring(0,daoXmlTemplate.lastIndexOf(dot)));
        generator.setTemplate(templateConfig);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setNaming(NamingStrategy.underline_to_camel);
        strategy.setColumnNaming(NamingStrategy.underline_to_camel);
        strategy.setSuperEntityClass(supperEntityFile);
        strategy.setSuperMapperClass(supperMapperFile);
        strategy.setEntityLombokModel(false);
        strategy.setRestControllerStyle(true);
        //strategy.setSuperControllerClass("com.cikers.ps.controller.MysqlController");
        strategy.setInclude(tableName.split(","));//表名
        // 设置继承的父类字段
        strategy.setSuperEntityColumns(new String[]{"id","updateTime","updateBy","createTime","createBy"});
        //strategy.setControllerMappingHyphenStyle(true);
        //strategy.setTablePrefix(pc.getModuleName() + "_");
        strategy.setTablePrefix("t_");
        generator.setStrategy(strategy);
        generator.setTemplateEngine(new FreemarkerTemplateEngine());
        generator.execute();
    }
}
