package ${package.Controller};

import ${package.Service}.${table.serviceName};
import ${package.Entity}.${entity};
import cn.afterturn.easypoi.excel.ExcelExportUtil;
import cn.afterturn.easypoi.excel.entity.ExportParams;
import java.io.IOException;
import cn.afterturn.easypoi.excel.ExcelImportUtil;
import cn.afterturn.easypoi.excel.entity.ImportParams;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.bind.annotation.GetMapping;
import org.apache.poi.ss.usermodel.Workbook;
import javax.servlet.http.HttpServletResponse;
import org.springframework.beans.factory.annotation.Autowired;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.PostMapping;
import com.ggpc.base.web.advice.RespMessage;
import com.ggpc.base.web.advice.ResponseMessage;
import com.ggpc.base.web.util.FileUtil;
import org.springframework.web.bind.annotation.RequestBody;
import java.util.Arrays;
import java.util.List;
<#if restControllerStyle>
import org.springframework.web.bind.annotation.RestController;
<#else>
import org.springframework.stereotype.Controller;
</#if>
<#if superControllerClassPackage??><#--cotroller的父类 BaseController-->
import ${superControllerClassPackage};
</#if>

/**
 * <#--${table.comment!} 表注释--> 控制层
 * @author ${author}
 * @since ${date}
 */
<#if restControllerStyle>
@RestController
<#else>
@Controller
</#if>
@RequestMapping("<#if package.ModuleName?? && package.ModuleName != "">/${package.ModuleName?replace("Entity","")}</#if>/<#if controllerMappingHyphenStyle??>${controllerMappingHyphen?replace("Entity","")}<#else>${table.entityPath?replace("Entity","")}</#if>")
<#if kotlin>
class ${table.controllerName}<#if superControllerClass??> : ${superControllerClass}()</#if>
<#else>
<#if superControllerClass??>
public class ${table.controllerName} extends ${superControllerClass} {
<#else>
public class ${table.controllerName} {

</#if>
    @Autowired
    private ${table.serviceName}   ${table.serviceName?uncap_first};
    /**
     * 新增
     */
     @PostMapping("/save")
     @RespMessage
     public ResponseMessage save(@RequestBody ${entity} ${entity?uncap_first}){
            boolean success = ${table.serviceName?uncap_first}.save(${entity?uncap_first});
            if(success){
                return ResponseMessage.ok();
            }
            return ResponseMessage.error();
     }
    /**
     * 修改
     */
    @PostMapping("/update")
    @RespMessage
    public ResponseMessage update(@RequestBody ${entity} ${entity?uncap_first}){
            boolean success = ${table.serviceName?uncap_first}.updateByPK(${entity?uncap_first});
            if(success){
                return ResponseMessage.ok();
            }
            return ResponseMessage.error().message("数据未更新或数据不存在");
     }
    /**
     * 删除
     */
    @PostMapping("/delete")
    @RespMessage
    public ResponseMessage delete(Long id){
            boolean success = ${table.serviceName?uncap_first}.deleteById(id);
            if(success){
                return ResponseMessage.ok();
            }
            return ResponseMessage.error();
     }
    /**
     * 主键查询
     */
    @PostMapping("/queryById")
    @RespMessage
    public ResponseMessage queryById(Long id){
            ${entity} ${entity?uncap_first} = ${table.serviceName?uncap_first}.queryById(id);
            if(${entity?uncap_first} == null){
                return ResponseMessage.ok().data(${entity?uncap_first});
            }
            return ResponseMessage.error().message("数据不存在");
     }
    /**
     * 通过主键集合查询
     */
    @PostMapping("/getList")
    @RespMessage
    public ResponseMessage getList(String ids){
            String[] idArr = ids.split(",");
            List list = Arrays.asList(idArr);
            List<${entity}> resultList = ${table.serviceName?uncap_first}.selectByIds(list);
            return ResponseMessage.ok().data(resultList);

     }
    /**
     * 分页查询
     */
    @PostMapping("/pageList")
    @RespMessage
    public ResponseMessage pageList(Page<${entity}> page,@RequestBody ${entity} queryParam){
            QueryWrapper<${entity}> wrapper = new QueryWrapper(queryParam);
            page = ${table.serviceName?uncap_first}.queryPageList(page,wrapper);
            return ResponseMessage.ok().data(page);

     }
    /**
     * 查询全部
     */
    @PostMapping("/getAll")
    @RespMessage
    public List<${entity}> getAll(){
        return  ${table.serviceName?uncap_first}.list();
     }
    /**
     * 导出全部
     */
    @GetMapping("/exportAll")
    @RespMessage
    public void exportAll(HttpServletResponse response){
        List<${entity}> list = ${table.serviceName?uncap_first}.list();
        ExportParams exportParams = new ExportParams();
        exportParams.setSheetName("${entity?replace("Entity","")}");
        Workbook workbook = ExcelExportUtil.exportExcel(exportParams, ${entity}.class, list);
        try {
            FileUtil.output(workbook,response,"${entity?replace("Entity","")}.xlsx");
        } catch (IOException e) {
            e.printStackTrace();
        }
     }
    /**
     * 导入
     */
    @PostMapping("/imports")
    @RespMessage
    public ResponseMessage imports(MultipartFile file){
        ImportParams importParams = new ImportParams();
        importParams.setTitleRows(0);
        importParams.setHeadRows(1);
        importParams.setSheetNum(1);
         try {
            List<${entity}> list = ExcelImportUtil.importExcel(file.getInputStream(),${entity}.class,importParams);
            boolean success = ${table.serviceName?uncap_first}.insertBatch(list);
            if(success){
                return ResponseMessage.ok();
            }
         } catch (Exception e) {

         }
         return ResponseMessage.error();
     }


}
</#if>
