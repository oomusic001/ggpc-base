package com.ggpc.base.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan("com.ggpc.base.service")
public class ServiceAutoConfig {
}
